{dev?true}:
  let drv = import ./default.nix { inherit dev; };
  in drv.steredis.env
