
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TupleSections     #-}

module Database.Steredis.Internal where

import Prelude                   hiding (dropWhile)
import Codec.Serialise                  (Serialise, serialise, deserialiseOrFail)
import Control.Exception                (Exception, SomeException, toException)
import Control.Monad                    ((<=<), void, when)
import Control.Monad.Catch              (onError)
import Control.Monad.Except             (runExceptT)
import Control.Monad.Failable
import Control.Monad.IO.Class           (MonadIO, liftIO)
import Control.Monad.Reader             (asks)
import Data.Bits                        (shiftR)
import Data.Bool                        (bool)
import Data.ByteString.Char8            (ByteString, dropWhile, pack)
import Data.Either                      (rights)
import Data.Hashes.FNV1a
import Data.IORef                       (readIORef)
import Data.List                        (foldl', sortOn)
import Data.Map.Strict           as Map (lookup)
import Data.Maybe                       (catMaybes)
import Data.Time.Clock                  (getCurrentTime)
import Data.Vector            as Vector ((!?))
import Database.Steredis.Types

import qualified Data.ByteString       as B
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy  as LB
import qualified Database.Redis        as R

keyHash :: ByteString -> KeyHash
keyHash = KeyHash . fnv1a64

valueHash :: ByteString -> ValueHash
valueHash = ValueHash . fnv1a32

hashBlock :: [(ByteString, ByteString)] -> BlockHash
hashBlock = BlockHash . hashBlock' . fmap (uncurry mappend) . sortOn fst
    where hashBlock' = foldl' hash fnvOffsetBasis32
          hash       = B.foldl' fnv1aHash32Base

hashKeyPrefix :: ByteString
hashKeyPrefix = "__#"

metaBlockPrefix :: ByteString
metaBlockPrefix = "__m"

bucketPrefix :: ByteString
bucketPrefix = "__s"

bucketKey :: Int -> ByteString -> ByteString
bucketKey shard bucket = bucketPrefix <> B8.snoc shardStr '/' <> bucket
  where shardStr = pack $ show shard

controlKey :: ByteString
controlKey = "__%control"

nodesKey :: ByteString
nodesKey = "__%nodes"

topologyKey :: ByteString
topologyKey = "__%location_s"

mightFail :: (Failable m) => m (Either R.Reply a) -> m a
mightFail = (hoist RedisError =<<)

assert :: (Failable m, Exception e) => (a -> Bool) -> e -> a -> m a
assert p err value = do
  bool (failure err) (return value) =<< return (p value)

shardTopology :: DBTransformer t m => t m Topology
shardTopology = liftIO . readIORef =<< asks topology

connectionIdsFor :: DBTransformer t m => Int -> t m [NodeId]
connectionIdsFor shard = connectionIdsIn =<< shardTopology
  where connectionIdsIn Topology {..} =
            hoist invalidShard indexes >>=
            assert (not . null) noConnections
          where indexes = shardsIndexes !? shard
        invalidShard () = InvalidShard shard
        noConnections   = NoNodesForShard shard

connectionsFor :: DBTransformer t m => [NodeId] -> t m [R.Connection]
connectionsFor ids = connectionsIn =<< shardTopology
  where connectionsIn Topology {..} = catMaybes <$> mapM lookupIndex ids
          where lookupIndex = return . flip Map.lookup knownRedis

-- run redis actions on all instances which host the given shard but return
-- only the first (succesful) result or if all failed, the first failure.
onEachRedis :: DBTransformer t m => Int -> R.Redis (Either R.Reply a) -> t m a
onEachRedis shard op =  mightFail $ do
  connections <- connectionsFor <=< connectionIdsFor $ shard
  results     <- mapM (runOnRedis op) connections
  case rights results of
    []  -> return $ head results -- return first if none succeeded (all are failures)
    x:_ -> return $ Right x      -- or first one that suceeds

-- run redis actions on all instances which host the given shard returning
-- all results
onAllRedis :: DBTransformer t m => Int
                                -> R.Redis (Either R.Reply a)
                                -> t m [(NodeId, Either SomeException a)]
onAllRedis shard op = do
  connectionIds <- connectionIdsFor shard
  connections   <- connectionsFor connectionIds
  zip connectionIds <$> mapM runOn connections
    where runOn = runExceptT . mightFail . runOnRedis op

-- run redis actions on one instance hosting the given shard
onOneRedis :: DBTransformer t m => Int -> R.Redis (Either R.Reply a) -> t m a
onOneRedis shard op = mightFail $ do
  connections <- connectionsFor <=< connectionIdsFor $ shard
  tryEachOneOf connections . toException $ NoNodesForShard shard
    where tryEachOneOf [] e = failure e
          tryEachOneOf (connection:rest) _ =
            runOnRedis op connection `recover` \e ->
              tryEachOneOf rest e

onRedis ::  DBTransformer t m => NodeId -> R.Redis (Either R.Reply a) -> t m a
onRedis iD op = mightFail . runOnRedis op =<< connectionIn =<< shardTopology
  where connectionIn Topology {..} = hoist unknownNode $ Map.lookup iD knownRedis
        unknownNode ()             = UnknownNode iD

runOnRedis :: (MonadIO m) => R.Redis a -> R.Connection -> m a
runOnRedis = ((.).(.)) liftIO $ flip R.runRedis

locationFor :: DBTransformer t m => ByteString -> t m Location
locationFor key = assigned . unKeyHash $ keyHash key
  where assigned hash =
          asks settings >>= \Knobs{..} -> do
            let shard = fromIntegral $ hash `shiftR` 32 `mod` numShards
                block = fromIntegral $ hash `mod` numBlocks
            bucket <- keyBucket shard key
            return (bucket, (shard, block))

runTransaction :: R.RedisTx (R.Queued a) -> R.Redis (Either R.Reply a)
runTransaction = fmap hoistTxResult . R.multiExec
  where hoistTxResult R.TxAborted     = Left . R.Error $ "Transaction aborted"
        hoistTxResult (R.TxSuccess v) = return v
        hoistTxResult (R.TxError msg) = Left . R.Error $ "Transaction error: " <> pack msg

updateWithMeta :: DBTransformer t m
               => ByteString
               -> Address
               -> Maybe ByteString
               -> R.RedisTx (R.Queued a)
               -> t m a
updateWithMeta key (shard, blockNum) mValue operation =
  updateWithMeta' >>= \case
    []  -> failure . AllNodesFailed $ "Updating " <> key
    x:_ -> return x
    where updateWithMeta' =
            retriable $ do
              watched <- fmap fst <$> startWatching address
              when (null watched) . failure $ ShardNotAvailable shard
              blocks <- computeBlock watched address key mValue
              mapM update (zip watched blocks) `onError` mapM stopWatching watched
              where startWatching = onAllRedis shard . R.watch . return
                    stopWatching  = flip onRedis R.unwatch
                    update (iD, (hash, block)) = do
                      onRedis iD . runTransaction $ do
                        updateMeta address block
                        updateHash shard blockNum hash
                        operation
                    address = metaLocation shard blockNum

deserialize :: (Failable m, Serialise a) => ByteString -> m a
deserialize = autohoist . deserialiseOrFail . LB.fromStrict

updateMeta :: ByteString -> [(ByteString, ByteString)] -> R.RedisTx ()
updateMeta = ((.).(.)) void R.hmset

updateHash :: Int -> Int -> BlockHash -> R.RedisTx ()
updateHash shard block hash = do
  let address = hashLocation shard
  void . R.hset address (pack $ show block) . LB.toStrict $ serialise hash

retriable :: DBTransformer t m => t m a -> t m a
retriable operation =
  asks settings >>= \Knobs {..} ->
    attempt numRetries $ toException noRetries
      where attempt 0 = failure . TransactionAborted
            attempt n = const $ operation `recover` attempt (n-1)
            noRetries = SketchyConfig "Transaction retries is set to zero"

computeBlock :: DBTransformer t m => [NodeId]
                                  -> ByteString
                                  -> ByteString
                                  -> Maybe ByteString
                                  -> t m [(BlockHash, [(ByteString, ByteString)])]
computeBlock watched address key mValue = do
  now <- failableIO getCurrentTime
  mapM (computeBlock' now) watched
  where computeBlock' now iD = do
          block0 <- onRedis iD $ R.hgetall address
          let block = (key, LB.toStrict $ serialise meta) : filter ((/= key) . fst) block0
              hash  = hashBlock block
              vHash = mValue >>= return . valueHash
              meta  = Meta vHash now
          return (hash, block)

metaLocation :: Int -> Int -> ByteString
metaLocation shard block = metaBlockPrefix <> pack s <> "_b_" <> pack b
  where s = show shard
        b = show block

hashLocation :: Int -> ByteString
hashLocation shard = hashKeyPrefix <> pack s
  where s = show shard

_multi :: R.Redis (Either R.Reply R.Status)
_multi = R.sendRequest ["MULTI"]

_exec :: R.Redis R.Reply
_exec = either id id <$> R.sendRequest ["EXEC"]

bucketize :: Int -> ByteString -> Maybe (ByteString, ByteString)
bucketize shard key0 = do
  (key', b2) <- B.unsnoc key0
  (key, b1)  <- B.unsnoc key'
  return (bucketKey shard key, b1 `B.cons` b2 `B.cons` B.empty)

keyBucket :: (Failable m) => Int -> ByteString -> m (ByteString, ByteString)
keyBucket = ((.).(.)) (hoist keyTooShort) bucketize

notFound :: (Failable m) => ByteString -> m a
notFound = failure . NotFound

keyTooShort :: e -> SomeException
keyTooShort _ = toException KeyTooShort

enableKeySpaceEvents :: DBTransformer t m => NodeId -> t m R.Status
enableKeySpaceEvents iD = onRedis iD $ R.configSet "notify-keyspace-events" "KEA"

subscription :: ByteString -> ByteString
subscription = mappend "__keyspace*__:"

subscribe :: [ByteString] -> R.PubSub
subscribe   = R.psubscribe . fmap subscription

getEventKeyName :: Event -> ByteString
getEventKeyName (Event _ _ key)      = key
getEventKeyName (UnknownEvent _ key) = key

parseEvent :: R.Message -> Event
parseEvent msg =
    let key = B.drop 1 $ dropWhile (/= ':') $ R.msgChannel msg
    in case R.msgMessage msg of
         "set"  -> Event KeyChange Set key
         "del"  -> Event KeyChange Deleted key
         "hset" -> Event MapKeyChange Set key
         "hdel" -> Event MapKeyChange Deleted key
         "sadd" -> Event SetChange Set key
         "srem" -> Event SetChange Deleted key
         msg'   -> UnknownEvent msg' key
