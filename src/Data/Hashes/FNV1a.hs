{-# LANGUAGE BangPatterns #-}
module Data.Hashes.FNV1a where

import Data.Bits       (xor)
import Data.ByteString (ByteString)
import Data.Word       (Word8, Word32, Word64)

import qualified Data.ByteString as B

fnvPrime32 :: Word32
fnvPrime32 = 16777619

fnvPrime64 :: Word64
fnvPrime64 = 1099511628211

fnvOffsetBasis32  :: Word32
fnvOffsetBasis32  = 2166136261

fnvOffsetBasis64 :: Word64
fnvOffsetBasis64 = 14695981039346656037

fnv1a64  :: ByteString -> Word64
fnv1a64 = B.foldl' fnv1aHash64Base fnvOffsetBasis64

fnv1aHash64Base :: Word64 -> Word8 -> Word64
fnv1aHash64Base !h !x = fnvPrime64 * (h  `xor` fromIntegral x)

fnv1a32 :: ByteString -> Word32
fnv1a32 = B.foldl' fnv1aHash32Base fnvOffsetBasis32

fnv1aHash32Base :: Word32 -> Word8 -> Word32
fnv1aHash32Base !h !x = fnvPrime32 * (h  `xor` fromIntegral x)