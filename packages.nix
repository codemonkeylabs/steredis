#
{dev, ...}: self: super: let

in {
  ghc = super.ghc;

  steredis = self.callCabal2nix "steredis" (
                 builtins.filterSource (path: type: type != "directory" ||
                                        baseNameOf path != ".git") ./. ) { };

  # overriden nixpkgs packages we need...

}
