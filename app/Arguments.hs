module Arguments where

import Control.Arrow            ((***))
import Data.Functor             ((<&>))
import Data.List.Split          (splitOn)
import Data.Tuple.Sequence      (sequenceT)
import Data.Maybe               (mapMaybe)
import Data.ByteString.Char8    (ByteString, pack)
import Database.Steredis        (parseAddr)
import Database.Steredis.Types
import Database.Redis           (PortID)
import Options.Applicative
import Options.Applicative.Types (readerAsk)
import GHC.Conc                 (numCapabilities)
import Text.Read                (readMaybe)

data Command = Init
             | Run [Test]
             | Scan

data Test = RW
          | BlockHashes
          deriving (Eq)

data Arguments = Arguments {
                    _command    :: Command                      ,
                    _nodes      :: [(NodeId, (String, PortID))] ,
                    _topology   :: [(Int, NodeId )]             ,
                    _on         :: Maybe NodeId                 ,
                    _numThreads :: Int                          ,
                    _numKeys    :: Int                          ,
                    _indexBits  :: Int                          ,
                    _numShards  :: Int                          ,
                    _pattern    :: ByteString                   ,
                    _chunkSize  :: Maybe Int
                 }

options :: Parser Arguments
options =
    subparser $  command "init" (info (initCommand <**> helper) (progDesc "Initialize database"))
              <> command "test" (info (testCommand <**> helper) (progDesc "Run tests"))
              <> command "scan" (info (scanCommand <**> helper) (progDesc "Scan (iterate) DB keys"))

defNumKeys :: Int
defNumKeys = 1000000

initCommand :: Parser Arguments
initCommand =
  Arguments Init [] [] Nothing
    <$> unused "num-threads"
    <*> unused "num-keys"
    <*> indexBitsOpt
    <*> numShardsOpt
    <*> pure ""
    <*> pure Nothing

scanCommand :: Parser Arguments
scanCommand =
  Arguments Scan
    <$> nodesOpt
    <*> topologyOpt
    <*> onOpt
    <*> numThreadsOpt
    <*> numKeysOpt
    <*> unused "index-bits"
    <*> unused "num-shards"
    <*> patternOpt
    <*> chunkSizeOpt

patternOpt :: Parser ByteString
patternOpt = pack <$> strOption  (long "pattern" <> short 'p' <> metavar "PATTERN" <> value "*" <> help "Glob style pattern to use in key scan operation")

chunkSizeOpt :: Parser (Maybe Int)
chunkSizeOpt = optional $ option auto (long "chunk-size" <> short 'c' <> metavar "INT" <> help "Maximum number of elements to process internally in one iteration")

nodesOpt :: Parser [(NodeId, (String, PortID))]
nodesOpt = option parseNodes (long "nodes" <> short 'n' <> metavar "ID:HOST:PORT,..." <> help "Provide list of known Redis nodes (i.e. 23:10.1.1.1:6379,57:[2c01::1]:7000 would configure two nodes with IDs 23 and 57, on 10.0.1.1 port 6379 and 2c01::1 port 7000 respectively)" <> value [])

topologyOpt :: Parser [(Int, NodeId)]
topologyOpt = option parseTopology (long "topology" <> short 'y' <> metavar "SHARD:ID,..." <> help "Configure mapping of shards to Redis instances (i.e. 1:23,2,23,3:57,4:57) would configure shards 1 & 2 to be on node 23 and 3 & 4 to be on node 57)" <> value [])

numThreadsOpt :: Parser Int
numThreadsOpt = option auto (long "threads" <> short 't' <> metavar "INT" <> value numCapabilities <> help "Specify how many threads should be spawn in parallel (default is -N RTS option)")

numKeysOpt :: Parser Int
numKeysOpt = option auto (long "keys" <> short 'k' <> metavar "INT" <> value defNumKeys <> help "Amount of keys to be used during tests")

onOpt :: Parser (Maybe NodeId)
onOpt =  fmap NodeId <$> (optional $ option auto (long "on" <> short 'o' <> metavar "ID" <> help "Run test on the node specified by the given id"))

indexBitsOpt :: Parser Int
indexBitsOpt  = option auto (long "index-bits" <> short 'i' <> metavar "INT" <> value 16 <> help "Number of metadata blocks per shard (must be a power of 2)" <> showDefault)

numShardsOpt :: Parser Int
numShardsOpt  = option auto (long "num-shards" <> short 's' <> metavar "INT" <> value 8 <> help "Number of DB shards" <> showDefault)

unused :: String -> Parser a
unused name = pure $ error ("illegal use of option " ++ name)

testCommand :: Parser Arguments
testCommand =
  Arguments . Run
    <$> parseTests
    <*> nodesOpt
    <*> topologyOpt
    <*> onOpt
    <*> numThreadsOpt
    <*> numKeysOpt
    <*> unused "index-bits"
    <*> numShardsOpt
    <*> pure ""
    <*> pure Nothing
      where parseTests    = some $ subparser testsCommands
            testsCommands = command "rw"          (info (rwTest <**> helper)          (progDesc "read/write test")) <>
                            command "blockhashes" (info (blockHashesTest <**> helper) (progDesc "block hashes test"))
            rwTest          = pure RW
            blockHashesTest = pure BlockHashes

parseNodes :: ReadM [(NodeId, (String, PortID))]
parseNodes = readerAsk <&> parseListWith parseNodeId parseAddr'

parseTopology :: ReadM [(Int, NodeId)]
parseTopology  = readerAsk <&> parseListWith readMaybe parseNodeId

parseNodeId :: String -> Maybe NodeId
parseNodeId = fmap NodeId . readMaybe

parseAddr' :: String -> Maybe (String, PortID)
parseAddr'  = parseAddr . pack

parseListWith :: (Show a, Show b) => (String -> Maybe a) -> (String -> Maybe b) -> String -> [(a, b)]
parseListWith readA readB = mapMaybe readPair . splitOn ","
  where readPair = sequenceT . (readA *** readB) . fmap (drop 1) . span (/= ':')
