{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Tests where

import Arguments
import Control.Applicative          (Alternative)
import Control.Arrow                ((|||))
import Control.Concurrent.Async     (async, wait)
import Control.Exception            (Exception)
import Control.Monad.Catch          (MonadMask)
import Control.Monad                (forM, forM_, guard, when)
import Control.Monad.Except         (runExceptT)
import Control.Monad.IO.Class       (MonadIO, liftIO)
import Control.Monad.Failable       (Failable, failableIO, hoist)
import Control.Monad.Trans.Control  (MonadBaseControl)
import Data.ByteString.Char8        (ByteString, pack)
import Data.Int                     (Int64)
import Data.Typeable                (Typeable)
import Database.Steredis
import Utils

data Error = MissingArgument String
           deriving (Show, Typeable)

instance Exception Tests.Error

runTests :: Arguments -> [Test] -> IO ()
runTests args tests = do
  startTime <- timeSinceStart
  result <- runExceptT $ do
    when (RW `elem` tests) $ rwTest args >>= uncurry reportRW
    when (BlockHashes `elem` tests) $ blockHashesTest args >>= uncurry reportBlockHashes
  failed ||| report startTime $ result
    where failed = putStrLn . ("FAILURE: " ++) . show
          report startTime _ = do
            now <- timeSinceStart
            let elapsed = (now - startTime) `div` 1000000
            putStrLn $ "Test(s) run in " ++ show elapsed ++ " mS"

rwTest :: (Failable m, MonadIO m) => Arguments -> m (Int64, Int)
rwTest args@Arguments{..} = do
  context <- connectionFor args
  startTime <- timeSinceStart
  liftIO . putStrLn $ "Running tests with " ++ show _numThreads ++ " threads"
  let spawnTest iD = failableIO . async $ do
                       forM_ [1..keysPerThread] $ withDatabase context . rwTest' prefix
                       return keysPerThread
                         where prefix = "t" <> pack (show iD) <> "_"
  written <- failableIO . mapM wait =<< forM [1.._numThreads] spawnTest
  return (startTime, foldr (+) 0 written)
    where keysPerThread     = _numKeys `div` _numThreads

reportRW :: (MonadIO m) => Int64 -> Int -> m ()
reportRW startTime numKeys = do
  now <- timeSinceStart
  let elapsed = (now - startTime) `div` 1000000
  liftIO . putStrLn $ "Wrote/read " ++ show numKeys ++ " key(s) in " ++ show elapsed ++ " mS"

rwTest' :: (Failable m, Alternative m, MonadIO m, MonadMask m) => ByteString
                                                               -> Int
                                                               -> OperationT m ()
rwTest' prefix n = do
  insert key str
  str' <- get key
  guard $ str == str'
    where key = prefix <> pack (show n)
          str = "dabale arroz a la zorra el abad"

blockHashesTest :: (Failable m, MonadIO m, MonadMask m, MonadBaseControl IO m)
                => Arguments -> m (Int64, Int)
blockHashesTest args = do
  context   <- connectionFor args
  startTime <- timeSinceStart
  iD <- hoist missingArgument $ _on args
  hashBlocks <- withDatabase context . forM [1.._numShards args] $ flip getBlockHashes iD
  return (startTime, fromIntegral . sum $ length <$> hashBlocks)
    where missingArgument _ = MissingArgument "--on <nodeId>"

reportBlockHashes :: (MonadIO m) => Int64 -> Int -> m ()
reportBlockHashes startTime numBlocks = do
  now <-timeSinceStart
  let elapsed = (now - startTime) `div` 1000000
  liftIO . putStrLn $ "Retrieved " ++ show numBlocks ++ " block hashes in "
    ++ show elapsed ++ " mS"
