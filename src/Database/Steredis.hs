{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ImplicitParams      #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE ScopedTypeVariables #-}
{- |
Module: Database.Steredis
Description: Redis on Steroids. Adaptation layer for space efficient storage of keys and
             metadata.
Copyright: (c) Erick Gonzalez, 2020
License: BSD3
Maintainer: erick@codemonkeylabs.de

Redis is an excellent work of software engineering. Nevertheless in order to deploy it in
high scale distributed applications, a few optimizations are indispensable to achieve higher
scalability numbers and enable functionality that requires associating metadata with the
keys stored. This library provides such an adaptation layer so that the details of this can
be abstracted and shared accross user applications.

-}
module Database.Steredis (
-- * How to use this module:
-- |
--
-- @
-- do
--   connection <- connect def
--   runExceptT . withDatabase connection $
--     x <- get "the x"
--     y <- get "the y"
--     return $ x <> y
-- @
--
-- I emphasize the use of the 'Failable' monad in this package. The library user has thus
-- the freedom to choose failure behaviour according to the connection on which the
-- database operations are run. This can however surprise the unattentive user who
-- runs 'withDatabase' in 'IO' and then gets bitten by an 'IO' exception being thrown
-- inadvertently etc. So you probably want to run operations with a monad transformer
-- that encapsulates the desired error behaviour (like 'ExceptT' perhaps, etc).
-- * Types
BlockHash(..),
Configuration(..),
Connection(..),
Cursor(..),
Error(..),
Event(..),
EventAction(..),
EventType(..),
NodeId(..),
OperationT,
-- * DB connection creation, management and execution of database actions
connect,
withDatabase,
addNode,
rmNode,
addShard,
rmShard,
resetTopology,
-- * Database operations
find,
get,
initialize,
info,
insert,
delete,
scan,
scanAll,
load,
store,
-- * Database Events API
enableKeySpaceEvents,
getEventKeyName,
waitForChanges,
-- * DB Metadata manipulation
getBlockHashes,
-- * Redis re-exported functions
R.subscribe,
R.unsubscribe,
-- * Utility functions
mkCursor,
parseAddr
) where

import Control.Applicative              ((<|>))
import Control.Arrow                    ((>>>))
import Control.Exception                (throw)
import Control.Monad                    ((<=<), guard, join, void, unless, when)
import Control.Monad.Catch              (MonadMask)
import Control.Monad.Failable
import Control.Monad.Fix                (fix)
import Control.Monad.IO.Class           (MonadIO)
import Control.Monad.Trans              (lift)
import Control.Monad.Reader             (asks)
import Control.Monad.Trans.Control      (MonadBaseControl)
import Control.Monad.Trans.Reader       (runReaderT)
import Control.Monad.Trans.Maybe        (MaybeT(..), runMaybeT)
import Codec.Serialise                  (DeserialiseFailure(..), Serialise, deserialiseOrFail, serialise)
import Data.Bifunctor                   (first)
import Data.Bitraversable               (bimapM)
import Data.Bits                        (shiftL)
import Data.ByteString.Char8            (ByteString, pack, unpack)
import Data.Conduit                     (ConduitT, awaitForever, fuseUpstream, yield)
import Data.Function                    ((&))
import Data.IORef                       (writeIORef, newIORef)
import Data.List                        (nub)
import Data.Maybe                       (catMaybes, fromMaybe)
import Data.Time.Clock                  (UTCTime(utctDayTime), diffTimeToPicoseconds, getCurrentTime)
import Data.Vector            as Vector (empty, fromList, toList)
import Data.Version                     (Version, makeVersion)
import Database.Steredis.Internal
import Database.Steredis.Types
import System.Random                    (mkStdGen, random)
import Text.Read                        (readMaybe)

import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy  as LB
import qualified Data.Map.Strict       as Map
import qualified Database.Redis        as R

instance Ord R.PortID where
  (<=) (R.PortNumber a) (R.PortNumber b) = a <= b
  (<=) a b = error $ "meaningless comparison between" ++ show a ++ " and " ++ show b

version :: Version
version = makeVersion [0,2,0]

minVersion :: Version
minVersion = makeVersion [0,2,0]

maxVersion :: Version
maxVersion = makeVersion [0,2,0]

-- | initialize DB control block.
initialize :: (MonadIO m, Failable m) => Configuration -> Int -> Int -> m ()
initialize Configuration {..} shards indexBits = failableIO $ do
  redisConn <- R.connect connectionInfo
  R.runRedis redisConn $ do
    success0 <- R.hsetnx controlKey "shards" . pack . show $ shards
    whenNot success0 $
      throw . InitFailure $ "Initializing shards in DB control block (already initialized?)"
    success1 <- R.hsetnx controlKey "index-bits" . pack . show $ indexBits
    whenNot success1 $
      throw . InitFailure $ "Unable to set metadata index bit length in control block"
    success2 <- R.hsetnx controlKey "version" . pack . show $ version
    whenNot success2 $
      throw . InitFailure $ "Unable to set DB version (already set?)"
        where whenNot = unless . either (const True) id

-- | create a @Connection@ for a Redis DB connection on which to perform DB actions. Note
-- that one may use 'Default' instance of 'Configuration' for set of default working settings
connect :: (MonadIO m, Failable m) => Configuration -> m Connection
connect config = do
  let ?config = config
  redisConn <- setupConnection
  let ?redisConn = redisConn
  connection <- usingControlData
  loadTopology connection
  return connection

updateConnectionWith :: (MonadIO m, Failable m) => IO (Either R.Reply ())
                                                -> Connection
                                                -> m ()
updateConnectionWith action connection@Connection{..} = do
  let ?redisConn = localRedis
  hoist RedisError =<< failableIO action
  loadTopology connection

-- | add a node address to the list of known Redis instances
addNode :: (MonadIO m, Failable m) => NodeId
                                   -> String
                                   -> R.PortID
                                   -> Connection
                                   -> m ()
addNode iD host (R.PortNumber port) connection@Connection {..} =
  updateConnectionWith addNode' connection
    where host'    = pack host
          port'    = pack $ show port
          idKey    = pack . show $ asWord iD
          addNode' = fmap void . R.runRedis localRedis $
                       R.hset nodesKey idKey ("[" <> host' <> "]:" <> port')
addNode _ _ port _ = failure . NotSupported $ " node on " ++ show port

-- | remove a node address from the list of known Redis instances
rmNode :: (MonadIO m, Failable m) => NodeId -> Connection -> m ()
rmNode iD connection@Connection {..} =
  updateConnectionWith rmNode' connection
    where rmNode' = fmap void . R.runRedis localRedis $ R.hdel nodesKey [idKey]
          idKey   = pack . show $ asWord iD

-- | assign a shard to the given Redis node instance
addShard :: (MonadIO m, Failable m) => Int -> NodeId -> Connection -> m ()
addShard = shardOp R.sadd

-- | remove a shard from the given node
rmShard :: (MonadIO m, Failable m) => Int -> NodeId -> Connection -> m ()
rmShard = shardOp R.srem

resetTopology :: (MonadIO m, Failable m) => Connection -> m ()
resetTopology Connection {..} =
  hoist RedisError =<< failableIO resetTopology'
    where resetTopology' = fmap void . R.runRedis localRedis $ do
            void $ R.del [nodesKey]
            mapM R.del =<< R.keys allShardTopologies
          allShardTopologies = topologyKey <> "*"

shardOp :: (MonadIO m, Failable m)
        => (ByteString -> [ByteString] -> R.Redis (Either R.Reply a))
        -> Int
        -> NodeId
        -> Connection
        -> m ()
shardOp op shard iD connection@Connection{..} =
  updateConnectionWith rmShard' connection
    where rmShard' = fmap void . R.runRedis localRedis $ op key [iDStr]
          iDStr     = pack . show $ asWord iD
          shardStr  = pack $ show shard
          key       = mappend topologyKey shardStr

setupConnection :: (MonadIO m, Failable m, ?config::Configuration) => m R.Connection
setupConnection = failableIO $ connectUsing connectionInfo
  where Configuration {..} = ?config
        connectUsing
          | checkConnect = R.checkedConnect
          | otherwise    = R.connect

usingControlData :: (MonadIO m, Failable m, ?config::Configuration, ?redisConn :: R.Connection)
                  => m Connection
usingControlData  = Connection <$> usingSettings <*> pure ?config <*> pure ?redisConn <*> noTopology
  where noTopology = failableIO $ newIORef Topology { knownRedis    = Map.empty,
                                                      shardsIndexes = Vector.empty }

usingSettings :: (MonadIO m, Failable m, ?config :: Configuration, ?redisConn :: R.Connection)
              => m Knobs
usingSettings = do
  params        <- hoist RedisError =<< fetchInfo
  shards :: Int <- hoist (badDB "shards") $ retrieve "shards" params
  indexBits     <- hoist (badDB "index-bits")  $ retrieve "index-bits" params
  dbVersion     <- hoist (badDB "version") $ retrieve "version" params
  when (dbVersion < minVersion || dbVersion > maxVersion) $
    failure . IncompatibleDBVersion $
      "DB version " ++ show dbVersion ++ " is outside compatible range >= "
        ++ show minVersion ++ " && <= " ++ show maxVersion
  return $ Knobs { numShards  = fromIntegral shards,
                   numBlocks  = 1 `shiftL` indexBits,
                   numRetries = transactionRetries }
    where Configuration {..} = ?config
          fetchInfo   = failableIO . R.runRedis ?redisConn $ R.hgetall controlKey
          badDB msg _ = BadDB $ "Failure reading " ++ msg
                          ++ " in control block. DB not initialized?"

loadTopology :: (MonadIO m, Failable m, ?redisConn :: R.Connection ) => Connection
                                                                     -> m ()
loadTopology Connection { settings = Knobs {..}, config = config@Configuration {..}, .. } = do
  knownRedis    <- fmap Map.fromList . mapM connectTo =<< redisNodes
  shardsIndexes <- Vector.fromList <$> mapM nodeIdsFor shards
  failableIO $ writeIORef topology Topology {..}
    where shards           = [0..numShards-1] :: [Int]
          redisNodes       = mapM loadNode =<< hoist RedisError =<< failableIO fetchNodes
          fetchNodes       = R.runRedis ?redisConn $ R.hgetall nodesKey
          loadNode         = bimapM parseId parseNodeAddr
          parseId          = return . NodeId <=< hoist badNodeId . readMaybe . unpack
          parseNodeAddr    = hoist badNodeAddr . parseAddr
          nodeIdsFor shard = fmap nub . mapM parseId =<<
                              hoist RedisError =<< failableIO fetchSet
            where fetchSet  = R.runRedis ?redisConn $
                                show shard          &
                                pack                &
                                mappend topologyKey &
                                R.smembers
          connectTo                  = mapM connectTo'
          connectTo' (address, port) = setupConnection
            where ?config = config { connectionInfo =
                                      connectionInfo { R.connectHost = address,
                                                       R.connectPort = port }
                                   }
          badNodeAddr () = BadDB "Unable to load node addresses"
          badNodeId   () = BadDB "Unable to load node IDs"

_genId :: (MonadIO m, Failable m) => [NodeId] -> m NodeId
_genId exists = fix genId'
  where genId' retry = do
          -- yes Mr. Schlaumeier, I know that this is not truly random but no, the NSA doesn't
          -- care about the internal IDs used to identify a connections, so use a pseudo random
          -- generator with current time as seed.
          iD <- fst.random.mkStdGen.fromIntegral.diffTimeToPicoseconds.utctDayTime <$> time
          if elem iD exists
            then retry
            else return iD
        time = failableIO getCurrentTime

parseAddr :: ByteString -> Maybe (String, R.PortID)
parseAddr str = parseBracketed <|> parseNormal
  where parseNormal = do
          let (host, rest) = B.span (/= ':') str
          port <- parsePort rest
          return (unpack host, port)
        parseBracketed = do
          let (host', rest') = B.span (/= ']') str
          host <- B.stripPrefix "[" host'
          rest <- B.stripPrefix "]" rest'
          port <- parsePort rest
          return (unpack host, port)
        parsePort =
          \case
            ""   -> return $ R.connectPort R.defaultConnectInfo
            str' -> return . R.PortNumber =<< readMaybe . unpack =<< B.stripPrefix ":" str'

retrieve :: (Read a) => ByteString -> [(ByteString, ByteString)] -> Maybe a
retrieve = ((.).(.)) readValue lookup
  where readValue = join . fmap (readMaybe . unpack)

-- | Run the specified DB operations on the given Redis DB connection
withDatabase :: (Failable m, MonadBaseControl IO m) => Connection -> OperationT m a -> m a
withDatabase conn action = runReaderT (runOperationT action) conn

-- | Retrieve DB control information parameter. Current parameters are
--     * version
--     * shards
--     * index-bits
info :: (DBTransformer t m, Read a) => ByteString -> t m a
info key = do
  connection <- asks localRedis
  params     <- mightFail . flip runOnRedis connection $ R.hgetall controlKey
  hoist (const $ NotFound key) $ retrieve key params

-- | Lookup key in the DB returning @Nothing@ if not found as opposed to a right out
-- operation failure (see 'get')
find :: DBTransformer t m => ByteString -> t m (Maybe ByteString)
find key = do
  (bucket, (shard, _)) <- locationFor key
  onOneRedis shard $ uncurry R.hget bucket

-- | Retrieve key from the DB. If not found, a failure is triggered according to the
-- underlying 'Failable' context; i.e. if 'IO' an exception would be thrown, if 'MaybeT'
-- 'Nothing' would be returned, etc.
get :: DBTransformer t m => ByteString -> t m ByteString
get key = maybe (notFound key) return =<< find key

-- | Insert a value under a given key in the DB
insert :: DBTransformer t m => ByteString -> ByteString -> t m ()
insert key value = do
  (bucket, address) <- locationFor key
  void . updateWithMeta key address (pure value) $ uncurry R.hset bucket value

-- | Remove key from the DB. Note that metadata is marked as deleted but is not actually
-- removed. Culling of deleted metadata entries no longer needed should be performed
-- periodically
delete :: DBTransformer t m => ByteString -> t m ()
delete key = do
  (bucket, address) <- first (fmap return) <$> locationFor key
  void . updateWithMeta key address Nothing $ uncurry R.hdel bucket

-- | Iterate over *all* keys matching the glob style pattern specified. The count parameter,
-- if specified, denotes the maximum number of entries that can be fed to the conduit at
-- one time.
scanAll :: DBTransformer t m => Maybe Int -> ByteString -> ConduitT () [ByteString] (t m) ()
scanAll count pattern = fix scanOnce Nothing
  where scanOnce iterateFrom cursor = do
          next <- fuseUpstream scanChunk feedChunk
          if catMaybes (Vector.toList $ unCursor next) == mempty
            then return ()
            else iterateFrom $ return next
            where scanChunk = scan count cursor pattern
                  feedChunk = awaitForever forChunk
                  forChunk chunk | null chunk = return ()
                                 | otherwise  = yield chunk

-- | Scan keys matching the specified pattern in the DB starting at the specified "cursor"
-- position (if not provided, from the start of the scan list) with a maximum count of
-- keys *as a hint* to Redis (if supplied). It returns the cursor position for the next entry
-- in the scan list.
scan :: DBTransformer t m => Maybe Int
                          -> Maybe Cursor
                          -> ByteString
                          -> ConduitT () [ByteString] (t m) Cursor
scan count provided pattern =
  fmap (Cursor . Vector.fromList) $ mapM scanOne =<< cursors <$> start
    where start   = maybe cursor0 return provided
          cursors = zip [0..] . Vector.toList . unCursor
          cursor0 =
            lift shardTopology >>= \Topology {..} ->
              return . Cursor $ selectOneOf <$> shardsIndexes
          scanOne                            = uncurry scanOne'
          scanOne' shard cursor = runMaybeT $ do
            (iD, rCursor)   <- autohoist cursor
            (next, results) <- lift . lift $ scanNode iD count rCursor bucketPattern
            lift $ scanBuckets iD count results
            guard $ next /= R.cursor0
            return (iD, next)
              where bucketPattern = bucketKey shard pattern
          selectOneOf []     = Nothing
          selectOneOf (iD:_) = return (iD, R.cursor0)

scanNode :: DBTransformer t m => NodeId
                              -> Maybe Int
                              -> R.Cursor
                              -> ByteString
                              -> t m (R.Cursor, [ByteString])
scanNode iD count cursor pattern = onRedis iD scanWithOptions
    where scanWithOptions = R.scanOpts cursor opts
          opts            = R.defaultScanOpts {
                              R.scanMatch = pure pattern,
                              R.scanCount = fromIntegral <$> count
                            }

scanBuckets :: DBTransformer t m => NodeId
                                 -> Maybe Int
                                 -> [ByteString]
                                 -> ConduitT () [ByteString] (t m) ()
scanBuckets iD count = mapM_ scanBucket
    where scanBucket key = do
            suffixes <- lift . onRedis iD $ R.hkeys key
            rawKey   <- lift . autohoist $
                          B.stripPrefix bucketPrefix key >>=
                          return . dropPrefix
            mapM yield . fix chunks $ mappend rawKey <$> suffixes
          chunks _       []    = []
          chunks process elems = let (x, rest) = splitAt maxItems elems in x : process rest
          maxItems             = fromMaybe maxBound count
          dropPrefix           = B.dropWhile (/= '/') >>> B.drop 1

-- | Retrieve a shard's block hash list. This can be used to figure out which blocks
-- have changed, for example.
getBlockHashes :: DBTransformer t m => Int -> NodeId -> t m [(Int, BlockHash)]
getBlockHashes shard iD = do
  let address = hashLocation shard
  hashes <- onRedis iD $ R.hgetall address
  mapM deserializePair hashes
    where deserializePair = bimapM asInt deserialize
          asInt bytes     = hoist (badData bytes) . readMaybe $ unpack bytes
          shard'          = pack $ show shard
          badData bytes _ =
            BadData ("invalid block "<> bytes <> " on shard "<> shard') $
              DeserialiseFailure 0 ""

-- | Helper function to retrieve and deserialize a previously stored type from the DB
load :: (Serialise a, DBTransformer t m) => ByteString -> t m a
load key = get key >>= decodeValue
  where decodeValue = hoist (BadData key) . deserialiseOrFail . LB.fromStrict

-- | Helper function to serialize and store a given type in the DB
store :: (Serialise a, DBTransformer t m) => ByteString -> a -> t m ()
store key = insert key . LB.toStrict . serialise

-- | Subscribe to Redis events for keys matching the pattern provided.
waitForChanges :: (Failable m, MonadIO m, MonadMask m) => [ByteString]
                                                       -> (Event -> IO R.PubSub)
                                                       -> OperationT m ()
waitForChanges patterns handler =
  waitForChangesOn =<< shardTopology
    where waitForChangesOn Topology {..} =
            mapM_ waitOnEach $ Map.keys knownRedis
          subscriptions = subscription <$> patterns
          handler'      = handler . parseEvent

          -- THIS HAS TO BE FORKED AND LINKED!!!!
          waitOnEach iD = do
            void $ enableKeySpaceEvents iD -- CHECK STATUS!!!
            onRedis iD $ return <$> R.pubSub (R.psubscribe subscriptions) handler'

-- | Return a Redis cursor from a bytestring representation
mkCursor :: (Failable m) => ByteString -> m R.Cursor
mkCursor = hoist RedisError . R.decode . R.Bulk . Just
