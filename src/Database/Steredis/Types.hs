{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

module Database.Steredis.Types where

import Codec.Serialise                  (Serialise, DeserialiseFailure)
import Control.Applicative              (Alternative)
import Control.Exception                (Exception, SomeException)
import Control.Monad.Catch              (MonadCatch, MonadMask, MonadThrow)
import Control.Monad.Failable
import Control.Monad.IO.Class           (MonadIO)
import Control.Monad.Reader             (MonadReader)
import Control.Monad.Trans              (MonadTrans)
import Control.Monad.Trans.Reader       (ReaderT)
import Data.Bits                        (Bits)
import Data.ByteString                  (ByteString)
import Data.IORef                       (IORef)
import Data.Time.Clock                  (UTCTime)
import Data.Default                     (Default(..))
import Data.Map.Strict           as Map (Map)
import Data.Typeable                    (Typeable)
import Data.Vector                      (Vector)
import Data.Word                        (Word32, Word64)
import GHC.Generics                     (Generic)
import System.Random                    (Random)

import qualified Database.Redis       as R

data Error = BadDB String                           -- ^ DB control or metadata missing or corrupt
           | IncompatibleDBVersion String           -- ^ Version of this library is not compatible with DB
           | InitFailure String
           | RedisError R.Reply                     -- ^ Redis library error
           | BadData ByteString DeserialiseFailure  -- ^ Deserialization error while loading data from DB
           | KeyTooShort                            -- ^ Key supplied is less than two bytes
           | NotFound ByteString                    -- ^ Key requested was not found in the DB
           | UnexpectedStatus String                -- ^ An operation returned a status other than what was expected
           | TransactionAborted SomeException       -- ^ A Redis transaction has been aborted (d'uh)
           | TransactionError String                -- ^ A command in a transaction returned error
           | NoNodesForShard Int                    -- ^ No nodes are configured or available for the given shard
           | ShardNotAvailable Int                  -- ^ The given shard can't be found on any known host
           | AllNodesFailed ByteString              -- ^ An operation on key failed on all nodes
           | InvalidShard Int                       -- ^ I refuse to write a tautological comment
           | UnknownNode NodeId                     -- ^ There is no connection configuration for the given node iD
           | SketchyConfig String                   -- ^ Configuration snafu
           | NotSupported String                    -- ^ Functionality is not .. supported
           deriving (Typeable, Show)

instance Exception Error

newtype NodeId = NodeId { asWord :: Word32 } deriving (Read, Show, Eq, Ord, Random)

data Configuration = Configuration { -- | Redis 'ConnInfo'
                                     connectionInfo :: R.ConnectInfo,
                                     -- | Specify whether a connection check should be made upon
                                     -- context creation (default True)
                                     checkConnect :: Bool,
                                     -- | Transaction retries - During periods of heavy access,
                                     -- it is possible for an internal transaction to update
                                     -- metadata to fail due to a collision. This setting
                                     -- specifies how many times that transaction should be
                                     -- reattempted
                                     transactionRetries :: Int }

instance Default Configuration where
  def = Configuration { connectionInfo     = R.defaultConnectInfo,
                        checkConnect       = True,
                        transactionRetries = 10 }

data Connection = Connection { settings   :: Knobs,
                               config     :: Configuration,
                               localRedis :: R.Connection,
                               topology   :: IORef Topology }

data Knobs = Knobs { numShards :: forall a . Integral a => a,
                     numBlocks :: forall a . (Integral a, Bits a) => a,
                     numRetries :: Int }

data Topology = Topology { shardsIndexes :: Vector [NodeId],
                           knownRedis    :: Map NodeId R.Connection }

newtype OperationT m a = OperationT { runOperationT :: ReaderT Connection m a }
  deriving (Functor,
            Alternative,
            Applicative,
            Monad,
            MonadIO,
            MonadReader Connection,
            MonadTrans,
            MonadCatch,
            MonadMask,
            MonadThrow,
            Failable)

newtype KeyHash   = KeyHash   {unKeyHash   :: Word64 } deriving (Generic, Show)
newtype ValueHash = ValueHash {unValueHash :: Word32 } deriving (Generic, Show)
newtype BlockHash = BlockHash {unBlockHash :: Word32 } deriving (Generic, Show)

instance Serialise KeyHash
instance Serialise ValueHash
instance Serialise BlockHash

type DBTransformer t m = (Failable (t m),
                          Failable m,
                          MonadIO m,
                          MonadIO (t m),
                          MonadReader Connection (t m),
                          MonadMask (t m))

data Meta = Meta (Maybe ValueHash) UTCTime deriving Generic

instance Serialise Meta

data EventType   = KeyChange
                 | MapKeyChange
                 | SetChange
                   deriving (Show, Generic)

instance Serialise EventType

data EventAction = Set
                 | Deleted
                   deriving (Show, Eq, Generic)

instance Serialise EventAction

type KeyName = ByteString

data Event = Event EventType EventAction KeyName
           | UnknownEvent ByteString KeyName
           deriving (Show, Typeable, Generic)

newtype Cursor = Cursor { unCursor :: Vector (Maybe (NodeId, R.Cursor)) }
               deriving (Eq, Semigroup, Monoid)

type Bucket   = (ByteString, ByteString)
type Address  = (Int, Int)
type Location = (Bucket, Address)

instance Serialise Event
instance Exception Event