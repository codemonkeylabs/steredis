module Utils where

import Arguments
import Control.Monad            (when)
import Control.Monad.Failable   (Failable)
import Control.Monad.IO.Class   (MonadIO, liftIO)
import Data.Default             (def)
import Data.Int                 (Int64)
import Database.Steredis
import System.Clock             (Clock(Monotonic), TimeSpec(..), getTime)

timeSinceStart :: MonadIO m => m Int64
timeSinceStart = liftIO $
    getTime Monotonic >>= \(TimeSpec secs ns) ->
        return $ secs * 1000000000 + ns

connectionFor :: (Failable m, MonadIO m) => Arguments -> m Connection
connectionFor Arguments {..} = do
  connection <- connect def
  configure connection
  return connection
  where configure connection = do
          when (not (null _nodes) || not (null _topology)) $ resetTopology connection
          mapM_ addOneNode _nodes
          mapM_ addOneShard _topology
            where addOneNode (iD, (host, port)) = addNode iD host port connection
                  addOneShard (shard, iD)       = addShard shard iD connection
