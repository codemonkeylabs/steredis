# Hello intrepid nixer. If you are here cause you need to override something or add
# a dependency for a project you are working in, this is definitely not where
# you should do that. See your project's "override.nix" file. If you are 
# a release master elder, then you _still_ probably are in the wrong place
# (see packages.nix or releases/xxx.json). If you still think you should be doing
# something here you are a level 10 nix elder and you know what you are doing so
# may the schwartz be with you.
# 
{dev?true}:
let
  localOverride = let file = ./override.nix;
                  in if builtins.pathExists file then import file else (_: _: {});

  base          = import <nixpkgs> {};
  pinnedVersion = base.pkgs.lib.importJSON ./nixpkgs.json;

  nixpkgs = base.pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    inherit (pinnedVersion) rev sha256;
  };

  localPackages =
    base.lib.composeExtensions
      (import ./packages.nix { inherit dev; }) localOverride;

  overlay = self : super : {
    localHaskellPackages = 
      super.haskell.packages.ghc8104.override (old: {
        overrides =
	  self.lib.composeExtensions (old.overrides or (_: _: {})) localPackages;
      });
  };

  pkgs = import nixpkgs {
    overlays = [overlay];
  };

  withEnv = drv : drv // {
              env = drv.env.overrideAttrs (
                      old: with pkgs.localHaskellPackages; {
                        shellHooks = "hpack";
                        nativeBuildInputs =
                          let devTools = if dev then [haskell-language-server hlint] else [];
                              runDeps  = [ pkgs.redis ];
                              tools    = [ cabal-install ];
                          in old.nativeBuildInputs ++ tools ++ devTools ++ runDeps;
                      });
            };

  callPackage = pkgs.lib.callPackageWith (pkgs // pkgs.xlibs );

  in with pkgs; {
    inherit redis;
    steredis = withEnv localHaskellPackages.steredis;
  }
