module Commands where

import Arguments
import Control.Monad.IO.Class   (liftIO)
import Data.ByteString.Char8    (unpack)
import Data.Conduit             (awaitForever, connect)
import Data.Default             (def)
import Database.Steredis hiding (connect)
import Utils

initDB :: Arguments -> IO ()
initDB Arguments{..} = do
  initialize def _numShards _indexBits
  liftIO $ putStrLn "OK"

scanKeys :: Arguments -> IO ()
scanKeys args@Arguments{..} = do
  context <- connectionFor args
  withDatabase context $ do
    scanAll _chunkSize _pattern `connect` awaitForever output
      where output = mapM_ $ liftIO . putStrLn . unpack
