{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Arguments
import Commands
import Tests                      (runTests)
import Options.Applicative

main :: IO ()
main = do
  args <- execParser opts
  case _command args of
    Init      -> initDB args
    Run tests -> runTests args tests
    Scan      -> scanKeys args
    where opts = info (options <**> helper)
                  (fullDesc
                  <> header "steredis - DB management tool")